
const FIRST_NAME = "Mihail Irinel";
const LAST_NAME = "Preda";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching()
{
  var emptyObj={};
  var cache={};
  cache.pageAccessCounter=function(websiteName='home')
  {
    websiteName=websiteName.toLowerCase();
    if(emptyObj[websiteName]===undefined)
    emptyObj[websiteName]=1;
    else
    emptyObj[websiteName]++;
  }
  cache.getCache=function()
  {
    return emptyObj;
  }
  return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

